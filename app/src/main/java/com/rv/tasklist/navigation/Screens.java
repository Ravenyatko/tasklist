package com.rv.tasklist.navigation;

import androidx.fragment.app.Fragment;
import com.rv.tasklist.ui.fragments.AuthFragment;
import com.rv.tasklist.ui.fragments.TasksListFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {

    public static final class AuthScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return new AuthFragment();
        }
    }

    public static final class TasksListScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return new TasksListFragment();
        }
    }
}