package com.rv.tasklist.data

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.rv.tasklist.data.callbacks.GenerateTokenListener
import com.rv.tasklist.data.network.GraphAPIManager

/**
 * Created on 17.02.2019.
 */
class AccessTokenManager(val context: Context) {

    companion object {
        private const val PREFS_NAME = "SecuredData"
        private const val KEY_TOKEN = "token"
    }

    private val mSharedPrefs: SharedPreferences
    private var mAccessToken: String? = null
    private val mApiDataManager = GraphAPIManager()

    init {
        mSharedPrefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
    }

    fun getToken(): String? {
        if (mAccessToken == null) {
            mAccessToken = getTokenFromPrefs()
        }
        return mAccessToken
    }

    fun saveToken(token: String) {
        mAccessToken = token
        saveTokenToPrefs(token)
    }

    private fun getTokenFromPrefs(): String? {
        val tokenInPrefs = mSharedPrefs.getString(KEY_TOKEN, null)
        return if (tokenInPrefs != null) {
            decodeToken(tokenInPrefs)
        } else null
    }

    private fun saveTokenToPrefs(token: String) {
        mSharedPrefs.edit().putString(KEY_TOKEN, encodeToken(token)).apply()
    }

    private fun decodeToken(data: String): String? {
        //todo add encoding
        return data
    }

    private fun encodeToken(data: String): String? {
        //todo add encoding
        return data
    }

    fun requestNewAccessToken(listener: GenerateTokenListener) {
        mApiDataManager.generateAccessToken(listener)
    }
}