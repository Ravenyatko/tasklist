package com.rv.tasklist.data.callbacks

import com.rv.tasklist.data.interfaces.ITask

/**
 * Created on 17.02.2019.
 */
interface TaskObserver {

    fun onTaskListChanged(tasks: List<ITask>)

    fun onTaskAdded(task: ITask)

    fun onTaskUpdated(task: ITask)

    fun onTaskDeleted(task: ITask)
}