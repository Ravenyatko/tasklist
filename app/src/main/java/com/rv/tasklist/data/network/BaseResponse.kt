package com.rv.tasklist.data.network

import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException

/**
 * Created on 14.02.2019.
 */
abstract class BaseResponse<Data> : ApolloCall.Callback<Data>() {

    companion object {
        const val TAG = "Response"
    }

    override fun onResponse(response: Response<Data>) {
        Log.d(TAG, response.data().toString())
    }

    override fun onFailure(e: ApolloException) {
        Log.e(TAG, e.message)
        if (e.message.equals("Access token is not valid")) {
            //todo token refresh
        }
    }
}