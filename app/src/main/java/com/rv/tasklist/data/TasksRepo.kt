package com.rv.tasklist.data

import android.os.Handler
import android.os.Looper
import com.rv.tasklist.TaskListApp
import com.rv.tasklist.data.callbacks.GetTasksListener
import com.rv.tasklist.data.callbacks.TaskListener
import com.rv.tasklist.data.callbacks.TaskObserver
import com.rv.tasklist.data.db.DBDataManager
import com.rv.tasklist.data.impl.TaskImpl
import com.rv.tasklist.data.interfaces.IDataSource
import com.rv.tasklist.data.interfaces.ITask
import com.rv.tasklist.data.network.GraphAPIManager
import com.rv.tasklist.utils.NetworkUtil
import java.util.concurrent.Executors

/**
 * Created on 14.02.2019.
 */
class TasksRepo : IDataSource, TaskListener {

    private val mApiDataManager = GraphAPIManager()
    private val mDbManager = DBDataManager()
    private val mTaskObservers = ArrayList<TaskObserver>()
    private val mHandler = Handler(Looper.getMainLooper())
    private val mBGExecutor = Executors.newSingleThreadExecutor()

    override fun fetchAllTasks() {
        getTasksFromDB()
        if (NetworkUtil.isNetworkConnected(TaskListApp.getInstance())) {
            getTasksFromAPI()
        }
    }

    private fun getTasksFromAPI() {
        mApiDataManager.getAllTasks(object : GetTasksListener {
            override fun onTasksFetched(tasks: List<ITask>) {
                mBGExecutor.execute { mDbManager.updateTasksListInDB(tasks) }
                notifyListenerOnMainThread { it.onTaskListChanged(tasks) }
            }

            override fun onNoTasksExists() {
                mBGExecutor.execute { mDbManager.clearTasksInDB() }
                notifyListenerOnMainThread { it.onTaskListChanged(ArrayList()) }
            }
        })
    }

    private fun getTasksFromDB() {
        mBGExecutor.execute {
            val items = mDbManager.getTasksFromDB()
            notifyListenerOnMainThread {
                it.onTaskListChanged(items.map { item ->
                    TaskImpl(
                        item.id,
                        item.name,
                        item.note,
                        item.isDone
                    )
                })
            }
        }
    }

    fun createNewTask(name: String, note: String?, isDone:Boolean) {
        if (NetworkUtil.isNetworkConnected(TaskListApp.getInstance())) {
            mApiDataManager.createTask(name, note, isDone, this)
        }
    }

    fun updateTaskStatus(id: String, status: Boolean) {
        if (NetworkUtil.isNetworkConnected(TaskListApp.getInstance())) {
            mApiDataManager.updateTaskStatus(id, status, this)
        }
    }

    fun deleteTask(id: String) {
        if (NetworkUtil.isNetworkConnected(TaskListApp.getInstance())) {
            mApiDataManager.deleteTask(id, this)
        }
    }

    override fun onTaskChanged(task: ITask, action: TaskListener.Action) {
        when (action) {
            TaskListener.Action.DELETED -> {
                mDbManager.deleteTask(task)
                notifyListenerOnMainThread { it.onTaskDeleted(task) }
            }
            TaskListener.Action.UPDATED -> {
                mDbManager.updateTask(task)
                notifyListenerOnMainThread { it.onTaskUpdated(task) }
            }
            TaskListener.Action.CREATED -> {
                mDbManager.insertTask(task)
                notifyListenerOnMainThread { it.onTaskAdded(task) }
            }
        }
    }

    fun subscribeForTasksChanges(listener: TaskObserver) {
        mTaskObservers.add(listener)
    }

    fun unsubscribeForTasksChanges(listener: TaskObserver) {
        mTaskObservers.remove(listener)
    }

    fun notifyListenerOnMainThread(action: (TaskObserver) -> Unit) {
        mHandler.post { mTaskObservers.forEach(action) }
    }

}