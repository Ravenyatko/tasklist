package com.rv.tasklist.data.callbacks

import com.rv.tasklist.AllTasksQuery

/**
 * Created on 14.02.2019.
 */
interface GenerateTokenListener {

    fun onAccessTokenProvided(accessToken: String)

}