package com.rv.tasklist.data.impl

import com.rv.tasklist.data.interfaces.ITask

/**
 * Created on 17.02.2019.
 */
class TaskImpl(val taskId: String, val taskName: String, val taskNote: String?, val isTaskDone: Boolean) : ITask {

    override fun getId(): String {
        return taskId
    }

    override fun getName(): String {
        return taskName
    }

    override fun getNote(): String? {
        return taskNote
    }

    override fun isDone(): Boolean {
        return isTaskDone
    }

    override fun equals(other: Any?): Boolean {
        return other is ITask && other.getId() == getId()
    }

}