package com.rv.tasklist.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasks")
data class TaskEntity(
    @PrimaryKey var id: String,
    @ColumnInfo var name: String,
    @ColumnInfo var note: String?,
    @ColumnInfo var isDone: Boolean
)