package com.rv.tasklist.data.network

import android.util.Log
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.rv.tasklist.*
import com.rv.tasklist.data.callbacks.GenerateTokenListener
import com.rv.tasklist.data.callbacks.GetTasksListener
import com.rv.tasklist.data.callbacks.TaskListener
import com.rv.tasklist.data.impl.TaskImpl
import com.rv.tasklist.data.interfaces.ITask
import com.rv.tasklist.utils.Consts
import okhttp3.OkHttpClient


/**
 * Created on 14.02.2019.
 */
class GraphAPIManager {

    private var apolloClient: ApolloClient = initApolloClient()

    private fun initApolloClient(): ApolloClient {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val originRequest = chain.request().newBuilder()
                val token = TaskListApp.getInstance().getAccessTokenManager().getToken()
                if(token!=null) {
                    val newRequest = originRequest.addHeader("Authorization", token)
                    chain.proceed(newRequest.build())
                } else {
                    chain.proceed(originRequest.build())
                }
            }
            .build()
        return ApolloClient.builder()
            .serverUrl(Consts.BASE_URL)
            .okHttpClient(okHttpClient)
            .build()
    }

    fun generateAccessToken(listener: GenerateTokenListener) {
        apolloClient.mutate(
            GenerateAccessTokenMutation.builder()
                .apiKey(Consts.API_KEY)
                .userName(Consts.API_USERNAME)
                .build()
        ).enqueue(object : BaseResponse<GenerateAccessTokenMutation.Data>() {
            override fun onResponse(response: Response<GenerateAccessTokenMutation.Data>) {
                val token = response.data()?.generateAccessToken()
                if (token != null) {
                    listener.onAccessTokenProvided(token)
                    Log.d("Token", response.toString())
                } else {
                    Log.d("Token", "Received empty token")
                }
            }
        })
    }

    fun getAllTasks(listener: GetTasksListener) {
        apolloClient.query(AllTasksQuery.builder().build())
            .enqueue(object : BaseResponse<AllTasksQuery.Data>() {
                override fun onResponse(response: Response<AllTasksQuery.Data>) {
                    val tasks = response.data()?.allTasks()
                    if (tasks == null || tasks.isEmpty()) {
                        listener.onNoTasksExists()
                    } else {
                        listener.onTasksFetched(parseTasks(tasks))
                    }
                }
            })
    }

    private fun parseTasks(data: List<AllTasksQuery.AllTask>): ArrayList<ITask> {
        val tasks = ArrayList<ITask>()
        data.forEach { item ->
            tasks.add(TaskImpl(item.id(), item.name(), item.note(), item.isDone))
        }
        return tasks
    }

    fun createTask(name: String, note: String?, isDone: Boolean, listener: TaskListener) {
        apolloClient.mutate(
            CreateTaskMutation.builder()
                .name(name)
                .note(note)
                .isDone(isDone)
                .build()
        ).enqueue(object : BaseResponse<CreateTaskMutation.Data>() {
            override fun onResponse(response: Response<CreateTaskMutation.Data>) {
                val task = response.data()?.createTask()
                if (task != null) listener.onTaskChanged(
                    TaskImpl(task.id(), task.name(), task.note(), task.isDone),
                    TaskListener.Action.CREATED
                ) else if (response.hasErrors()) {
                    onFailure(ApolloException(response.errors()[0].message()))
                }
            }
        })
    }

    fun updateTaskStatus(id: String, isDone: Boolean, listener: TaskListener) {
        apolloClient.mutate(
            UpdateTaskStatusMutation.builder()
                .id(id)
                .isDone(isDone)
                .build()
        ).enqueue(object : BaseResponse<UpdateTaskStatusMutation.Data>() {
            override fun onResponse(response: Response<UpdateTaskStatusMutation.Data>) {
                val task = response.data()?.updateTaskStatus()
                if (task != null) listener.onTaskChanged(
                    TaskImpl(task.id(), task.name(), task.note(), task.isDone),
                    TaskListener.Action.UPDATED
                )
            }
        })
    }

    fun deleteTask(id: String, listener: TaskListener) {
        apolloClient.mutate(
            DeleteTaskMutation.builder()
                .id(id)
                .build()
        ).enqueue(object : BaseResponse<DeleteTaskMutation.Data>() {
            override fun onResponse(response: Response<DeleteTaskMutation.Data>) {
                val result = response.data()?.deleteTask()
                if (result != null && result) listener.onTaskChanged(
                    TaskImpl(id, "", null, false),
                    TaskListener.Action.DELETED
                )
            }
        })
    }
}