package com.rv.tasklist.data.callbacks

import com.rv.tasklist.data.interfaces.ITask

/**
 * Created on 14.02.2019.
 */
interface GetTasksListener {

    fun onTasksFetched(tasks: List<ITask>)

    fun onNoTasksExists()
}