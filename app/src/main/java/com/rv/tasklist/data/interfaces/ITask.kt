package com.rv.tasklist.data.interfaces

/**
 * Created on 17.02.2019.
 */
interface ITask {

    fun getId(): String

    fun getName(): String

    fun getNote(): String?

    fun isDone(): Boolean

}