package com.rv.tasklist.data.interfaces

/**
 * Created on 17.02.2019.
 */
interface ITaskActions {

    fun onNewTaskClick()

    fun onCheckedChanged(task: ITask, isDone: Boolean)

    fun onDeleteTask(task: ITask)
}