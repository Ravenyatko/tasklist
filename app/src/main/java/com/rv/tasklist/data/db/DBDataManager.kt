package com.rv.tasklist.data.db

import androidx.room.Room
import com.rv.tasklist.TaskListApp
import com.rv.tasklist.data.interfaces.ITask

/**
 * Created on 17.02.2019.
 */
class DBDataManager {

    private val mDatabase: AppDatabase = Room.databaseBuilder(
        TaskListApp.getInstance(),
        AppDatabase::class.java, "task-list"
    ).build()

    fun getTasksFromDB(): List<TaskEntity> {
        return getTasksDao().getAll()
    }

    fun updateTasksListInDB(tasks: List<ITask>) {
        clearTasksInDB()
        getTasksDao().insertAll(prepareTasksEntities(tasks))
    }

    fun updateTask(task: ITask) {
        getTasksDao().update(createTaskEntity(task))
    }

    fun insertTask(task: ITask) {
        getTasksDao().insert(createTaskEntity(task))
    }

    fun deleteTask(task: ITask) {
        getTasksDao().delete(createTaskEntity(task))
    }

    private fun prepareTasksEntities(tasks: List<ITask>): ArrayList<TaskEntity> {
        val parsed = ArrayList<TaskEntity>()
        tasks.forEach { item -> parsed.add(createTaskEntity(item)) }
        return parsed
    }

    private fun createTaskEntity(task: ITask): TaskEntity {
        return TaskEntity(task.getId(), task.getName(), task.getNote(), task.isDone())
    }

    fun clearTasksInDB() {
        getTasksDao().deleteAll()
    }

    private fun getTasksDao(): TaskDao {
        return mDatabase.tasksDao()
    }
}