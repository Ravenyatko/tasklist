package com.rv.tasklist.data.interfaces

/**
 * Created on 14.02.2019.
 */
interface IDataSource {

    fun fetchAllTasks()

}