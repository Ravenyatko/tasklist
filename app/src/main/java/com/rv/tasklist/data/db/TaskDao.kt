package com.rv.tasklist.data.db

import androidx.room.*


@Dao
interface TaskDao {

    @Query("SELECT * FROM tasks")
    fun getAll(): List<TaskEntity>

    @Query("SELECT * FROM tasks WHERE id = (:taskId)")
    fun getById(taskId: String): TaskEntity

    @Insert
    fun insert(task: TaskEntity)

    @Update
    fun update(task: TaskEntity)

    @Transaction
    fun insertAll(objects: List<TaskEntity>) = objects.forEach { insert(it) }

    @Delete
    fun delete(task: TaskEntity)

    @Query("DELETE FROM tasks")
    fun deleteAll()
}