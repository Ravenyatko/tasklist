package com.rv.tasklist.data.callbacks

import com.rv.tasklist.data.interfaces.ITask

/**
 * Created on 14.02.2019.
 */
interface TaskListener {

    enum class Action {
        CREATED, UPDATED, DELETED
    }

    fun onTaskChanged(task: ITask, action: Action)

}