package com.rv.tasklist.ui.mvp.views

import com.arellomobile.mvp.MvpView
import com.rv.tasklist.data.interfaces.ITask

interface TasksListView : MvpView {

    fun showList(tasks: List<ITask>)

    fun addItem(task: ITask)

    fun redrawItem(task: ITask)

    fun deleteItem(task: ITask)

    fun showEmptyState()

    fun showNewTaskInput()

    fun hideNewTaskInput()
}