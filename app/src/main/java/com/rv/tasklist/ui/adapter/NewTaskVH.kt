package com.rv.tasklist.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.rv.tasklist.data.interfaces.ITaskActions

/**
 * Created on 17.02.2019.
 */
class NewTaskVH(itemVIew: View, listener: ITaskActions) : RecyclerView.ViewHolder(itemVIew) {

    init {
        itemVIew.setOnClickListener { listener.onNewTaskClick() }
    }
}