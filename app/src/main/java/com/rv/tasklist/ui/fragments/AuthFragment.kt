package com.rv.tasklist.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.rv.tasklist.R
import com.rv.tasklist.ui.mvp.presenters.AuthPresenter
import com.rv.tasklist.ui.mvp.views.AuthView


/**
 * Created on 17.02.2019.
 */
class AuthFragment : BaseFragment(), AuthView {

    @InjectPresenter
    lateinit var mPresenter: AuthPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }
}