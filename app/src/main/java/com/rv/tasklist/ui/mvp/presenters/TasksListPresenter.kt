package com.rv.tasklist.ui.mvp.presenters

import com.arellomobile.mvp.InjectViewState
import com.rv.tasklist.data.TasksRepo
import com.rv.tasklist.data.callbacks.TaskObserver
import com.rv.tasklist.data.interfaces.ITask
import com.rv.tasklist.ui.mvp.views.TasksListView

@InjectViewState
class TasksListPresenter : BasePresenter<TasksListView>(), TaskObserver {

    val mTasksRepo = TasksRepo()
    var mTasksList: List<ITask>? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        mTasksRepo.fetchAllTasks()
    }

    override fun attachView(view: TasksListView?) {
        super.attachView(view)
        mTasksRepo.subscribeForTasksChanges(this)
    }

    override fun detachView(view: TasksListView?) {
        mTasksRepo.unsubscribeForTasksChanges(this)
        super.detachView(view)
    }

    override fun onTaskListChanged(tasks: List<ITask>) {
        mTasksList = tasks
        if (tasks.isEmpty()) {
            viewState.showEmptyState()
        } else {
            viewState.showList(tasks)
        }
    }

    override fun onTaskAdded(task: ITask) {
        viewState.addItem(task)
    }

    override fun onTaskUpdated(task: ITask) {
        viewState.redrawItem(task)
    }

    override fun onTaskDeleted(task: ITask) {
        viewState.deleteItem(task)
    }

    fun createNewTaskClicked() {
        viewState.showNewTaskInput()
    }

    fun addNewTask(name: String, note: String?, isDone: Boolean) {
        viewState.hideNewTaskInput()
        mTasksRepo.createNewTask(name, note, isDone)
    }

    fun changeTaskStatus(task: ITask, isDone: Boolean) {
        mTasksRepo.updateTaskStatus(task.getId(), isDone)
    }

    fun deleteTask(task: ITask) {
        mTasksRepo.deleteTask(task.getId())
    }

}