package com.rv.tasklist.ui.adapter

import android.view.ContextMenu
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rv.tasklist.R
import com.rv.tasklist.data.interfaces.ITask
import com.rv.tasklist.data.interfaces.ITaskActions

/**
 * Created on 17.02.2019.
 */
class TaskVH(itemVIew: View, val listener: ITaskActions) : RecyclerView.ViewHolder(itemVIew),
    View.OnCreateContextMenuListener {

    private var task: ITask? = null

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        if (task != null) {
            menu.add(0, v.id, 0, v.context.getString(R.string.delete))
                .setOnMenuItemClickListener {
                    listener.onDeleteTask(task!!)
                    true
                }
        }
    }

    private val mCheckBox: CheckBox = itemVIew.findViewById(R.id.task_cb)
    private val mListener = listener

    fun bindTask(task: ITask) {
        this.task = task
        mCheckBox.text = task.getName()
        mCheckBox.isChecked = task.isDone()
        mCheckBox.setOnCheckedChangeListener { _, isChecked -> mListener.onCheckedChanged(task, isChecked) }
        itemView.setOnCreateContextMenuListener(this)
    }
}