package com.rv.tasklist.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.rv.tasklist.R
import com.rv.tasklist.data.interfaces.ITask
import com.rv.tasklist.data.interfaces.ITaskActions
import com.rv.tasklist.ui.adapter.TasksAdapter
import com.rv.tasklist.ui.mvp.presenters.TasksListPresenter
import com.rv.tasklist.ui.mvp.views.TasksListView
import kotlinx.android.synthetic.main.fragment_tasks_list.*


/**
 * Created on 17.02.2019.
 */
class TasksListFragment : BaseFragment(), TasksListView, ITaskActions {

    @InjectPresenter
    lateinit var mPresenter: TasksListPresenter
    lateinit var mAdapter: TasksAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tasks_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        initViews()
    }

    private fun initViews() {
        create_new_task_fab.setOnClickListener { mPresenter.createNewTaskClicked() }
        tasks_rv.layoutManager = LinearLayoutManager(activity)
        mAdapter = TasksAdapter(this)
        tasks_rv.adapter = mAdapter
    }

    override fun showList(tasks: List<ITask>) {
        empty_layout.visibility = View.GONE
        tasks_view.visibility = View.VISIBLE
        mAdapter.setTasks(tasks)
    }

    override fun addItem(task: ITask) {
        mAdapter.addTask(task)
    }

    override fun redrawItem(task: ITask) {
        mAdapter.updateTask(task)
    }

    override fun deleteItem(task: ITask) {
        mAdapter.deleteTask(task)
    }

    override fun showEmptyState() {
        empty_layout.visibility = View.VISIBLE
        tasks_view.visibility = View.GONE
    }

    override fun showNewTaskInput() {
        empty_layout.visibility = View.GONE
        tasks_view.visibility = View.VISIBLE
        new_task_row.visibility = View.VISIBLE
        new_task_et.requestFocus()
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(new_task_et, InputMethodManager.SHOW_IMPLICIT)
        activity?.invalidateOptionsMenu()
    }

    override fun hideNewTaskInput() {
        new_task_row.visibility = View.GONE
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(new_task_et.windowToken, 0)
        activity?.invalidateOptionsMenu()
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        menu?.findItem(R.id.done)?.isVisible = new_task_row.visibility == View.VISIBLE
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.new_task, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.done -> mPresenter.addNewTask(new_task_et.text.toString(), null, new_task_cb.isChecked)
        }
        return true
    }

    override fun onNewTaskClick() {
        mPresenter.createNewTaskClicked()
    }

    override fun onCheckedChanged(task: ITask, isDone: Boolean) {
        if (task.isDone() != isDone)
            mPresenter.changeTaskStatus(task, isDone)
    }

    override fun onDeleteTask(task: ITask) {
        mPresenter.deleteTask(task)
    }

}