package com.rv.tasklist.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rv.tasklist.R
import com.rv.tasklist.data.interfaces.ITask
import com.rv.tasklist.data.interfaces.ITaskActions

/**
 * Created on 17.02.2019.
 */
class TasksAdapter(val listener: ITaskActions) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_ITEM = 1
        const val TYPE_NEW = 2
    }

    private val mTasks = ArrayList<ITask>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> TaskVH(
                LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false),
                listener
            )
            else -> NewTaskVH(
                LayoutInflater.from(parent.context).inflate(R.layout.item_new_task, parent, false),
                listener
            )
        }
    }

    override fun getItemCount() = mTasks.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TaskVH -> holder.bindTask(mTasks[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == itemCount - 1) {
            return TYPE_NEW
        } else
            return TYPE_ITEM
    }

    fun setTasks(tasks: List<ITask>) {
        mTasks.clear()
        mTasks.addAll(tasks)
        notifyDataSetChanged()
    }

    fun addTask(task: ITask) {
        mTasks.add(task)
        notifyItemInserted(mTasks.size - 1)
    }

    fun updateTask(task: ITask) {
        val index = mTasks.indexOf(task)
        mTasks.remove(task)
        mTasks.add(index, task)
        notifyItemChanged(mTasks.indexOf(task))
    }

    fun deleteTask(task: ITask) {
        val index = mTasks.indexOf(task)
        mTasks.remove(task)
        notifyDataSetChanged()
    }
}