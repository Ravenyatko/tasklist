package com.rv.tasklist.ui.mvp.presenters

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.rv.tasklist.TaskListApp
import ru.terrakok.cicerone.Router

/**
 * Created on 17.02.2019.
 */
abstract class BasePresenter<T : MvpView> : MvpPresenter<T>() {

    private val mRouter = TaskListApp.getInstance().getRouter()

    protected fun getRouter(): Router {
        return mRouter
    }
}