package com.rv.tasklist.ui.mvp.presenters

import android.os.Handler
import android.os.Looper
import com.arellomobile.mvp.InjectViewState
import com.rv.tasklist.TaskListApp
import com.rv.tasklist.data.callbacks.GenerateTokenListener
import com.rv.tasklist.navigation.Screens
import com.rv.tasklist.ui.mvp.views.AuthView

@InjectViewState
class AuthPresenter : BasePresenter<AuthView>(), GenerateTokenListener {

    private val mHandler = Handler(Looper.getMainLooper())
    private val accessTokenManager = TaskListApp.getInstance().getAccessTokenManager()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        val accessToken = accessTokenManager.getToken()
        if (accessToken == null) {
            accessTokenManager.requestNewAccessToken(this)
        } else goToMainScreen()
    }

    override fun onAccessTokenProvided(accessToken: String) {
        accessTokenManager.saveToken(accessToken)
        goToMainScreen()
    }

    private fun goToMainScreen() {
        mHandler.post { getRouter().replaceScreen(Screens.TasksListScreen()) }
    }
}