package com.rv.tasklist.ui

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.rv.tasklist.R
import com.rv.tasklist.TaskListApp
import com.rv.tasklist.navigation.Screens
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace

class MainActivity : MvpAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            navigator.applyCommands(arrayOf(Replace(Screens.AuthScreen())))
        }
    }

    private val navigator = object : SupportAppNavigator(this, R.id.main_container) {
        override fun applyCommands(commands: Array<Command>) {
            super.applyCommands(commands)
            supportFragmentManager.executePendingTransactions()
        }
    }

    override fun onStart() {
        super.onStart()
        TaskListApp.getInstance().getNavigatorHolder().setNavigator(navigator)
    }

    override fun onStop() {
        super.onStop()
        TaskListApp.getInstance().getNavigatorHolder().removeNavigator(

        )
    }

}
