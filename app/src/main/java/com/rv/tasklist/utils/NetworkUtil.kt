package com.rv.tasklist.utils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created on 14.02.2019.
 */
class NetworkUtil {

    companion object {

        fun isNetworkConnected(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        }
    }

}