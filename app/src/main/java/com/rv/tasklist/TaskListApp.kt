package com.rv.tasklist

import android.app.Application
import com.rv.tasklist.data.AccessTokenManager
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router


/**
 * Created on 14.02.2019.
 */
class TaskListApp : Application() {

    private lateinit var cicerone: Cicerone<Router>
    private lateinit var accessTokenManager: AccessTokenManager

    companion object {

        private lateinit var mAppInstance: TaskListApp

        fun getInstance(): TaskListApp {
            return mAppInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        mAppInstance = this
        accessTokenManager = AccessTokenManager(this)
        cicerone = Cicerone.create()
    }

    fun getNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }

    fun getRouter(): Router {
        return cicerone.router
    }

    fun getAccessTokenManager(): AccessTokenManager {
        return accessTokenManager
    }
}
